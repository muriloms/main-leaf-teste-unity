using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : SingletonMonobehaviour<AudioManager>
{
    [Header("Musica para trilha sonora")]
    [SerializeField] private AudioClip trilhaSonora;
    
    [Header("UI Sounds")]
    [SerializeField] private AudioClip btnConfirm;
    [SerializeField] private AudioClip btnSelect;
    
    [Header("Collectables Sounds")]
    [SerializeField] private AudioClip btnCoin;
    
    [Header("Audio Source")]
    [SerializeField] private AudioSource sourceTrilhaSonora;
    [SerializeField] private AudioSource uiSounds;
    [SerializeField] private AudioSource collectableSound;
    
    // Start is called before the first frame update
    void Start()
    {
        if (trilhaSonora != null)
        {
            sourceTrilhaSonora.clip = trilhaSonora;
            sourceTrilhaSonora.loop = true;
            sourceTrilhaSonora.Play();
        }
    }

    public void SoundUIConfirm()
    {
        if (btnConfirm != null)
        {
            uiSounds.clip = btnConfirm;
            uiSounds.Play();
        }
    }
    
    public void SoundUISelect()
    {
        if (btnSelect != null)
        {
            uiSounds.clip = btnSelect;
            uiSounds.Play();
        }
    }
    public void SoundCoin()
    {
        if (btnConfirm != null)
        {
            collectableSound.clip = btnCoin;
            collectableSound.Play();
        }
    }
    
}
