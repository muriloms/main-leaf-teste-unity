
using UnityEngine;

public class Coin : MonoBehaviour
{
    [Header("Settings variables")] 
    [SerializeField] private int coinValue;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            AudioManager.Instance.SoundCoin();
            LevelManager.Instance.Coin(coinValue);
            Destroy(gameObject);
        }
    }
}
