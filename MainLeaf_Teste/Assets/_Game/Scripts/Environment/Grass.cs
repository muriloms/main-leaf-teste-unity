﻿
using UnityEngine;

public class Grass : MonoBehaviour
{
    [SerializeField] private ParticleSystem fxHit;
    private bool isCut;

    void GetHit(int amount)
    {
        if (isCut == false)
        {
            isCut = true;
            transform.localScale = new Vector3(1f, 1f, 1f);
            fxHit.Emit(10);
        }
    }
    
}