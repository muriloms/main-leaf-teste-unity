
using UnityEngine;
public abstract class Menu<T> : Menu where T : Menu<T>
    {
        // reference to public and private instances
        private static T _instance;
        public static T Instance { get { return _instance; } }
        

        // self-destruct if another instance already exists
        protected virtual void Awake()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                _instance = (T)this;
            }
            
        }

        // unset the instance if this object is destroyed
        protected virtual void OnDestroy()
        {
            _instance = null;
        }
        
        // simplifies syntax to open a menu
        public static void Open()
        {
            
            if (MenuManager.Instance != null && Instance != null)
            {
                MenuManager.Instance.OpenMenu(Instance);
            }
            
        }

    }

    // base class for Menus
    [RequireComponent(typeof(Canvas))]
    public abstract class Menu : MonoBehaviour
    {
        public void SoundSelectBtn()
        {
            AudioManager.Instance.SoundUISelect();
        }
        
        public virtual void OnBackPressed()
        {
            AudioManager.Instance.SoundUIConfirm();
            
            if (MenuManager.Instance != null)
            {
                MenuManager.Instance.CloseMenu();
            }
            
        }

        
    }
