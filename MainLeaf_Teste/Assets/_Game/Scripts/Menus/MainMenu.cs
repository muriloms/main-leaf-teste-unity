
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : Menu<MainMenu>
{
    [SerializeField] private string sceneNameToPlayGame;
    // open Play Screen
    public void OnPlayGame()
    {
        AudioManager.Instance.SoundUIConfirm();
        SceneManager.LoadScene(sceneNameToPlayGame);
    }

    // open the CreditsPressed
    public void OnCreditsPressed()
    {
        AudioManager.Instance.SoundUIConfirm();
        
        CreditsMenu.Open();
    }

    public override void OnBackPressed()
    {
        base.OnBackPressed();
        Application.Quit();
#if UNITY_EDITOR

        UnityEditor.EditorApplication.isPlaying = false;

#endif
    }
}
