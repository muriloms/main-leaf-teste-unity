
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour
{
    [Header("Settings variables")] 
    [SerializeField] private float movementSpeed = 3f;
    [SerializeField] private float boxMovementSpeed = 1.5f;
    [SerializeField] private float crouchMovementSpeed = 2f;
    [SerializeField] private float jumpForce = 4f;
    
    [Header("Insert game objects")]
    [SerializeField] private Transform cameraTransform;

    private PlayerStates _playerStates;
    
    // Walk movement
    private float speed;
    private Vector3 direction;
    private float magnitude;
    private Vector3 velocity;
    
    // Jump movement
    private float ySpeed;

    // Start is called before the first frame update
    void Start()
    {
        _playerStates = GetComponent<PlayerStates>();
        speed = movementSpeed;
    }

    private void Update()
    {
        UpdatePlayerSpeed();
    }


    /*
     * Função para movimentar o personagem
     * de acordo com os input do teclado
     */
    public Vector3 Movement(float horizontal, float vertical)
    {
        // Calcular vetor direction normalizando valor entre 0 a 1
        direction = new Vector3(horizontal, 0f, vertical);
        magnitude = Mathf.Clamp01(direction.magnitude) * speed;

        direction = Quaternion.AngleAxis(cameraTransform.rotation.eulerAngles.y, Vector3.up) * direction;
        direction.Normalize();
        velocity = direction * magnitude;
        
        // Rotacionar personagem
        if (direction.magnitude > 0.1f)
        {
            if (!PlayerStates.Instance.IsMoveBox)
            {
                float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.Euler(0, targetAngle, 0);
            }
            
        }

        return velocity;
    }

    /*
     * Retornar força do pulo
     */
    public float Jump()
    {
        return jumpForce;
    }

    /*
     * Atualizar velocidade do player
     */
    private void UpdatePlayerSpeed()
    {
        if (_playerStates.IsMoveBox)
        {
            speed = boxMovementSpeed;
        }
        else if (_playerStates.IsCrouch)
        {
            speed = crouchMovementSpeed;
        }
        else
        {
            speed = movementSpeed;
        }
    }
}
