
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Settings variables Gravity and Ground Collision")] 
    [SerializeField] private float gravityScale;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask whatIsGround;
    
    [Header("Settings variables crouch")] 
    [SerializeField] private float centerYCharController;
    [SerializeField] private float heightCharController;

    private CharacterController _characterController;
    private PlayerMovement _playerMovement;
    private PlayerStates _playerStates;

    // Variaveis para o movimento
    private Vector3 velocity;
    private float gravity;
    private bool isGrounded;
    
    // Variaveis para coletar inputs de movimento
    private float horizontalMov;
    private float verticalMov;

    // Variaveis do tamanho dos colisores do player
    private float defaultCenterYCharController;
    private float defaultHeightCharController;

    // Start is called before the first frame update
    void Start()
    {
        gravity = Physics.gravity.y;
        
        _characterController = GetComponent<CharacterController>();
        _playerMovement = GetComponent<PlayerMovement>();
        _playerStates = GetComponent<PlayerStates>();

        defaultCenterYCharController = _characterController.center.y;
        defaultHeightCharController = _characterController.height;
    }

    private void FixedUpdate()
    {
        // Verificar se o player esta em contato com o chao
        isGrounded = Physics.CheckSphere(groundCheck.position, 0.1f, whatIsGround);
    }

    // Update is called once per frame
    void Update()
    {
        if (_playerStates.IsCanPlay)
        {
            UpdateVelocity();
        
            // Movimentar player
            _characterController.Move(velocity * Time.deltaTime);

            Crouch();
        }
    }

    /*
     * Atualizar valores de velocity para mover o player
     */
    private void UpdateVelocity()
    {
        horizontalMov = Input.GetAxis("Horizontal"); 
        verticalMov = Input.GetAxis("Vertical"); 

        if (verticalMov == 0 && horizontalMov == 0) // Player esta andando?
        {
            _playerStates.IsWalk = false;
        }
        else
        {
            _playerStates.IsWalk = true;
        }
        
        // Limitar valor do y no velocity quando estiver no chao
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }
        
        velocity.x = _playerMovement.Movement(horizontalMov, verticalMov).x;
        velocity.z = _playerMovement.Movement(horizontalMov, verticalMov).z;

        // Realizar pulo, somente se houver input, estiver no chao e não estiver movendo a caixa
        if (Input.GetButtonDown("Jump") && isGrounded && !_playerStates.IsMoveBox)
        {
            velocity.y = _playerMovement.Jump();
        }
  

        velocity.y += gravity * gravityScale * Time.deltaTime;
    }

    /*
     * Mecânica de agachar (crounch)
     */
    private void Crouch()
    {
        if (Input.GetMouseButton(1))
        {
            _characterController.center = new Vector3(_characterController.center.x,centerYCharController, _characterController.center.z);
            _characterController.height = heightCharController;
            _playerStates.IsCrouch = true;
        }
        else
        {
            _characterController.center = new Vector3(_characterController.center.x,defaultCenterYCharController, _characterController.center.z);
            _characterController.height = defaultHeightCharController;
            _playerStates.IsCrouch = false;
        }
    }

    
}
