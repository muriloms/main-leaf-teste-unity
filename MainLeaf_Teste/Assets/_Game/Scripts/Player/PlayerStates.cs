
using UnityEngine;

public class PlayerStates : SingletonMonobehaviour<PlayerStates>
{
    public bool IsCanPlay { get; set; }
    public bool IsWalk { get; set; }
    public bool IsJump { get; set; }
    public bool IsCrouch { get; set; }
    public bool IsMoveBox { get; set; }
    public bool IsDead { get; set; }
    
    // Start is called before the first frame update
    void Start()
    {
        IsWalk = false;
        IsJump = false;
        IsCrouch = false;
        IsMoveBox = false;
        IsDead = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0) && IsMoveBox)
        {
            IsMoveBox = false;
        }
    }
}
