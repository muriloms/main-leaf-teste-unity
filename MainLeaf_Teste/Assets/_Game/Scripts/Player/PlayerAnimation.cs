
using UnityEngine;


[RequireComponent(typeof(Animator))]
public class PlayerAnimation : MonoBehaviour
{
    private Animator m_Animator;
    private PlayerStates _playerStates;
    
    // Anim strings
    private static readonly int AnimIsWalk = Animator.StringToHash("isWalk");
    private static readonly int AnimIsDead = Animator.StringToHash("isDead");
    private static readonly int AnimPushBox = Animator.StringToHash("PushBox");
    private static readonly int AnimIsCrouch = Animator.StringToHash("isCrouch");

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        _playerStates = GetComponent<PlayerStates>();
    }

    // Update is called once per frame
    void Update()
    {
        m_Animator.SetBool(AnimIsWalk, _playerStates.IsWalk);
        m_Animator.SetBool(AnimIsDead, _playerStates.IsDead);
        m_Animator.SetBool(AnimIsCrouch, _playerStates.IsCrouch);
        
        if(!_playerStates.IsCanPlay) m_Animator.SetBool(AnimIsWalk, false);
        
        
    }
}
