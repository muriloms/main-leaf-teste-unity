
using UnityEngine;

public class PlayerCarryBox : MonoBehaviour
{
    [Header("Settings variables")] 
    [SerializeField] private float speedMoveBox;
    [SerializeField] private float radius;
    [SerializeField] private LayerMask whatIsBox;
    
    [Header("Insert game objects")]
    [SerializeField] private GameObject canvasBoxInfo;
    [SerializeField] private Transform handPosition;

    private Collider hitColliders;

    private bool isBox;
    private Ray ray;
    private RaycastHit hit;

    // Update is called once per frame
    void FixedUpdate()
    {
        isBox = Physics.CheckSphere(handPosition.position, radius, whatIsBox);
    }
    
    private void Update()
    {
        CheckBoxAndMove();
    }

    private void CheckBoxAndMove()
    {
        if (isBox)
        {
            canvasBoxInfo.SetActive(true);
            if (Input.GetMouseButton(0))
            {
                PlayerStates.Instance.IsMoveBox = true;
                RaycastHit[] box = Physics.SphereCastAll(handPosition.position, radius, Vector3.forward);
                if (box != null)
                {
                    for (int i = 0; i < box.Length; i++)
                    {
                        if (box[i].collider.gameObject.tag == "Box")
                        {
                            Vector3 direction = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
                            direction = Quaternion.AngleAxis(transform.rotation.eulerAngles.y, Vector3.up) * direction;
                            direction.Normalize();
                        
                            box[i].transform.Translate(direction * speedMoveBox * Time.deltaTime);
                        }
                    }
                }
            }

        }
        else
        {
            canvasBoxInfo.SetActive(false);
            PlayerStates.Instance.IsMoveBox = false;

        }
    }
    
    void OnDrawGizmosSelected()
    {
        if (handPosition != null)
        {
            // Draws a blue sphere from this transform to the target
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(handPosition.position,radius);
        }
    }
}
