
using System.Collections;
using TMPro;
using UnityEngine;

public class LevelManager : SingletonMonobehaviour<LevelManager>
{
    [Header("Check points")] 
    [SerializeField] private Transform pointReturnAfterGameOver;
    
    [Header("Screens")] 
    [SerializeField] private GameObject HUD;
    [SerializeField] private GameObject pauseScreen;
    [SerializeField] private GameObject gameOver;
    [SerializeField] private GameObject transitionScreen;
    
    [Header("Cameras")] 
    [SerializeField] private GameObject camThirdPerson;
    [SerializeField] private GameObject camMoveBox;
    [SerializeField] private GameObject[] camsPart2;
    
    [Header("HUD")] 
    [SerializeField] private TMP_Text txtCoins;
    

    private PlayerStates _playerStates;
  
    // Game states
    public bool IsGuardDetectPlayer;
    public bool IsPauseGame;
    public bool IsGameOver;
    public bool IsPart2;
    
    // Coin
    private int coins;

    protected override void Awake()
    {
        base.Awake();
        
        SetConfigMouse(true);
    }

    // Start is called before the first frame update
    void Start()
    {
        transitionScreen.SetActive(true);
        _playerStates = FindObjectOfType<PlayerStates>();
        _playerStates.IsCanPlay = true;
        
        // HUD
        coins = 0;
        txtCoins.text = coins.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape) && !IsPauseGame && !IsGameOver)
        {
            PauseGame();
        }

        if (!IsPauseGame)
        {
            if (!IsPart2)
            {
                if (_playerStates.IsMoveBox || IsGameOver)
                {
                    SetCamPart1(thirdPersonCam: false);
                }
                else
                {
                    SetCamPart1();
                }
            }
        
        
            if (IsGuardDetectPlayer && !IsGameOver)
            {
                GameOver();
            }
        }
        
    }

    /*
     * Ativar ou desativar cameras da parte 1 do game (antes dos guardas)
     */
    public void SetCamPart1(bool thirdPersonCam = true, bool deactiveAll = false)
    {
        if (!deactiveAll)
        {
            if (thirdPersonCam)
            {
                camThirdPerson.SetActive(true);
                camMoveBox.SetActive(false);
            }
            else
            {
                camThirdPerson.SetActive(false);
                camMoveBox.SetActive(true);
            }
        }
        else
        {
            camThirdPerson.SetActive(false);
            camMoveBox.SetActive(false);
        }
        
    }
    
    /*
     * Ativar ou desativar cameras da parte 2 do game (guardas)
     */
    public void SetCamPart2(int id = 0, bool deactiveAll = false)
    {
        if (!deactiveAll)
        {
            for (int i = 0; i < camsPart2.Length; i++)
            {
                if (i == id)
                {
                    camsPart2[i].SetActive(true);
                }
                else
                {
                    camsPart2[i].SetActive(false);
                }
            }

            IsPart2 = true;
            SetCamPart1(deactiveAll: true);
        }
        else
        {
            for (int i = 0; i < camsPart2.Length; i++)
            {
                camsPart2[i].SetActive(false);
            }

            IsPart2 = false;
        }
       
    }

    /*
     * Atualizar valor das moedas (coins)
     */
    public void Coin(int value)
    {
        coins += value;
        txtCoins.text = coins.ToString();
    }

    /*
     * Pausar game
     */
    private void PauseGame()
    {
        SetConfigMouse(false);
        
        _playerStates.IsCanPlay = false;
        IsPauseGame = true;
        
        HUD.SetActive(false);
        pauseScreen.SetActive(true);
        
        SetCamPart1(thirdPersonCam: false);
    }

    /*
     * Retornar game depois da pausa
     */
    public void ReturnPause()
    {
        SetConfigMouse(true);
        
        _playerStates.IsCanPlay = true;
        _playerStates.IsWalk = false;
        IsPauseGame = false;
        HUD.SetActive(true);
        
        SetCamPart1();
    }
    
    /*
     * Game over - player foi visto pelo guarda
     */
    private void GameOver()
    {
        SetConfigMouse(false);
        
        _playerStates.IsCanPlay = false;
        _playerStates.IsDead = true;
        IsGameOver = true;
        
        HUD.SetActive(false);
        gameOver.SetActive(true);
        
        SetCamPart1(thirdPersonCam: false);
    }

    /*
     * Retornar após game over - retornar no check point após primeio puzzle
     */
    public void ReturnAfterGameOver()
    {
        transitionScreen.SetActive(true);
        
        // Move play to target position
        _playerStates.gameObject.transform.position = pointReturnAfterGameOver.transform.position;
        
        SetConfigMouse(true);
        
        HUD.SetActive(true);
        
        // Cam parte 1
        SetCamPart1();

        // Cam parte 2
        SetCamPart2(deactiveAll: true);
        
        // Set game states
        IsGuardDetectPlayer = false;
        _playerStates.IsDead = false;

        // Esperar player mudar de posição para retornar os states de jogo
        StartCoroutine(SetStatesAfterReturn());

    }

    IEnumerator SetStatesAfterReturn()
    {
        yield return new WaitForSeconds(0.5f);
        IsGameOver = false;
        _playerStates.IsCanPlay = true;
    }

    /*
     * Player ganhou - chegou no check point final
     */
    public void WinGame()
    {
        LevelManager.Instance.SetCamPart1(thirdPersonCam: false);
        _playerStates.IsCanPlay = false;
        _playerStates.IsWalk = false;
    }
    

    /*
     * Configurações do mouse na tela
     */
    private void SetConfigMouse(bool isPlayGame)
    {
        if (isPlayGame)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }
    }
}
