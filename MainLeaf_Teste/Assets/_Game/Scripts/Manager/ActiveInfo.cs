
using UnityEngine;

public class ActiveInfo : MonoBehaviour
{
    [SerializeField] private GameObject objInfo;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            objInfo.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            objInfo.SetActive(false);
        }
    }
}
