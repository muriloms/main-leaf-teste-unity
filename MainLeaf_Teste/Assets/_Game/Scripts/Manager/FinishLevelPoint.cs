
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishLevelPoint : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
            
            LevelManager.Instance.WinGame();

            StartCoroutine(WaitBeforeChangeScene());
        }
    }

    IEnumerator WaitBeforeChangeScene()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("VictoryScreen");
    }
    
}
