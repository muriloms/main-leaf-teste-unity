
using UnityEngine;

public class ActiveCamera : MonoBehaviour
{

    [SerializeField] private int idCam;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            LevelManager levelManager = FindObjectOfType<LevelManager>();
            levelManager.SetCamPart2(idCam);
        }
    }
}
