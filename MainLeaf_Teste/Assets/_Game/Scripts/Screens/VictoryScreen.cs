
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryScreen : Screen
{
    [SerializeField] private string sceneLevelName;
    public void OnPlayAgain()
    {
        AudioManager.Instance.SoundUIConfirm();
        SceneManager.LoadScene(sceneLevelName);
    }
}
