
using UnityEngine;
using UnityEngine.SceneManagement;

public class Screen : MonoBehaviour
{
    public void OnRestart()
    {
        AudioManager.Instance.SoundUIConfirm();
        Scene m_scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(m_scene.name);
    }
    
    public void OnMainMenu()
    {
        AudioManager.Instance.SoundUIConfirm();
        SceneManager.LoadScene("MainMenu");
    }
    
    public void OnExit()
    {
        AudioManager.Instance.SoundUIConfirm();
        Application.Quit();
        
#if UNITY_EDITOR

        UnityEditor.EditorApplication.isPlaying = false;

#endif
    }
    
    public void SoundSelectBtn()
    {
        AudioManager.Instance.SoundUISelect();
    }
}
