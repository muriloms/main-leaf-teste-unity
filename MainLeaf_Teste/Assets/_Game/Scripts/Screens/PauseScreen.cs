
public class PauseScreen : Screen
{
    
    public void OnReturn()
    {
        AudioManager.Instance.SoundUIConfirm();
        gameObject.SetActive(false);
        LevelManager.Instance.ReturnPause();
    }
    
}
