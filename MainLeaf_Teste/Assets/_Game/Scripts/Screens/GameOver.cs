

public class GameOver : Screen
{
    public void OnReturnCheckPoint()
    {
        LevelManager.Instance.ReturnAfterGameOver();
        gameObject.SetActive(false);
    }
}
