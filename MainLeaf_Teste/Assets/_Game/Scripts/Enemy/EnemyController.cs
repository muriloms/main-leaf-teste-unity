
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [Header("Guard Config")]
    [SerializeField] private Transform[] waypoints;
    [SerializeField] private float _speed = 2f;
    [SerializeField] private float waitTime = 1f; // segundos
    [SerializeField] private EnemyDetectPlayer detectPlayer;
    
    private float _waitCounter = 0f;
    private bool _waiting = false;
    private int _currentWaypointIndex = 0;

    private LevelManager _levelManager;

    private Animator anim;
    private static readonly int AnimIsWalk = Animator.StringToHash("isWalk");

    private void Start()
    {
        _levelManager = FindObjectOfType<LevelManager>();
        anim = GetComponent<Animator>();
    }

    private void Update()
   {
       if (!_levelManager.IsGameOver && !_levelManager.IsPauseGame)
       {
           EnemyMovement();
       }
       
   }

    /*
     * Atualizar movimento e tempo de para do enemy
     */
   private void EnemyMovement()
   {
       if (_waiting) // Se estiver parado
       {
           anim.SetBool(AnimIsWalk, false);
           _waitCounter += Time.deltaTime;
           if (_waitCounter < waitTime)
               return;
           _waiting = false;
       }

       Transform wp = waypoints[_currentWaypointIndex];
       if (Vector3.Distance(transform.position, wp.position) < 0.01f) // Se chegou ao ponto, esperar
       {
           transform.position = wp.position;
           _waitCounter = 0f;
           _waiting = true;

           _currentWaypointIndex = (_currentWaypointIndex + 1) % waypoints.Length;
       }
       else // se não chegou ao ponto, mover
       {
           anim.SetBool(AnimIsWalk, true);
           transform.position = Vector3.MoveTowards(transform.position, wp.position, _speed * Time.deltaTime);
           transform.LookAt(wp.position);
       }
   }
}
