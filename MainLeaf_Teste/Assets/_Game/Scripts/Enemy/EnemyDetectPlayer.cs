
using UnityEngine;

public class EnemyDetectPlayer : MonoBehaviour
{
    public bool IsDetectPlayer;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            LevelManager levelManager = FindObjectOfType<LevelManager>();
            levelManager.IsGuardDetectPlayer = true;
            IsDetectPlayer = true;
        }
    }
}
