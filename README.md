# Main Leaf - Teste Unity

Projeto de desenvolvimento de mecânicas para um jogo 3D na engine Unity para avaliar as habilidades em programação e desenvolvimento de jogos como parte do processo seletivo da Main Leaf

## Arquivos

- [Build](https://gitlab.com/muriloms/main-leaf-teste-unity/-/tree/main/Build)
Contém arquivo .rar com a build do jogo (arquivos da Unity) e o instalar (executável) do jogo.

- [MainLeaf_Teste](https://gitlab.com/muriloms/main-leaf-teste-unity/-/tree/main/MainLeaf_Teste)
Projeto da Unity do jogo, com todos os códigos e materiais utilizados.

